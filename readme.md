# Getting Started

This document is intended to get you started developing a new handler. It provides some links to helpfull utilities and documentation. Before you release you are supposed to replace this content with your handler's documenation. See the 'Before you release' section for more details. 

## Software Development Kit

Before you can start developing, you will need a reference to the MessageHandler SDK. It's available on [https://www.myget.org/F/messagehandler/api/v2](https://www.myget.org/F/messagehandler/api/v2).
It's advised to add this Uri as a package source for your Nuget Package Manager (Tools > Options > Nuget Package Manager > Package Sources)

## Visual Studio Templates

To speed up the development of common handler patterns, as well as to provide learning material, we offer a visual studio extension with common project and item templates. You can find them on [https://www.myget.org/F/messagehandler/vsix/](https://www.myget.org/F/messagehandler/vsix/).
It's advise that you install these in your visual studio environment (Tools > Options > Environment > Extension and Updates > Additional Extension Galleries)

## Documentation

Detailed documentation about handler development can be found on our documentation website, under the section [Developing Handlers](http://www.messagehandler.net/documentation/handlers/developing-handlers)

## Examples & sources

Example implementations (as well as the sources for our SDK) can be found on [Github](https://github.com/MessageHandler).
* [Average In Period](https://github.com/MessageHandler/MessageHandler.Handlers.AverageInPeriod): shows how to build a regular processing unit that consist of a standing query and corresponding action.
* [Twitter Stream](https://github.com/MessageHandler/MessageHandler.Handlers.TwitterStream): shows how to stream messages from a message source.
* [Split files into messages](https://github.com/MessageHandler/MessageHandler.Handlers.SplitFileIntoMessages): shows how combine a processing unit and stream to deal with processing large file contents.

## Before release

When you are done developing and testing your handler, you are supposed to replace this content by the documentation of your handler, which should include the following items:

* A definition of the behavior of your handler, aka does it do?
* Documentation about the configuration settings
* Message format definitions that it accepts and emits.